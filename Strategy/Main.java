public class Main{
	public static void main(String[] args){
		NotaFiscal nota = new NotaFiscal();
		Imposto rj = new ImpostoRJ();
		Imposto sp = new ImpostoSP();
		nota.setImposto(rj);
		nota.calculaImposto();
		nota.setImposto(sp);
		nota.calculaImposto();
	}
}