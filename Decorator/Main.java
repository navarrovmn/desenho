public class Main{
	public static void main(String[] args){
		Arma arma = new ArmaBase();
		ArmaDecorator silenciador = new Silenciador(arma);
		ArmaDecorator mira = new Mira(arma);
		silenciador.montar();
		mira.montar();
	}
}