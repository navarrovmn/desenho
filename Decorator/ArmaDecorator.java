public class ArmaDecorator extends Arma {
	public Arma arma;

	public ArmaDecorator(Arma arma){
		this.arma = arma;
	}

	public void montar(){
		this.arma.montar();
	}

}