public class Silenciador extends ArmaDecorator{
	public Silenciador(Arma arma){
		super(arma);
	}
	public void montar(){
		System.out.println("Adicionando silenciador a arma");
	}
}