public class Mira extends ArmaDecorator {
	public Mira(Arma arma){
		super(arma);
	}
	public void montar(){
		System.out.println("Adicionando mira a arma");
	}
}