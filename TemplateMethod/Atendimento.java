public abstract class Atendimento {
	public abstract void recebePaciente();
	public abstract void informaData();
	public abstract void despedePaciente();
	public void atende(){
		recebePaciente();
		informaData();
		despedePaciente();
	}
}