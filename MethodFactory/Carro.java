public class Carro {
	private String name;
	private String color;
	private String modelo;

	public Carro(String name, String color, String modelo){
		this.name = name;
		this.color = color;
		this.modelo = modelo;
	}

	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}

	public void setColor(String color){
		this.color = color;
	}
	public String getColor(){
		return color;
	}

	public void setModelo(String modelo){
		this.modelo = modelo;
	}

	public String getModelo(){
		return modelo;
	}

}
