import java.util.*;

public class Main{
  public static void main(String[] args){
    Comida s = new Sanduiche("Sanduiche de Manteiga");
    Comida pao = new Pao("Pao com Parmesao");
    Comida manteiga = new Manteiga("Manteiga Light");
    System.out.println("Comida: " + s.getName());
    s.adiciona(pao);
    s.adiciona(manteiga);
    s.mostra();
  }
}
