import java.util.*;

public class Sanduiche extends Comida {
  ArrayList<Comida> componentes = new ArrayList<Comida>();
  public Sanduiche(String name){
    super(name);
  }
  public void adiciona(Comida comida){
    componentes.add(comida);
  }
  public void mostra(){
    for(Comida ingrediente : componentes){
      System.out.println("O componente e " + ingrediente.getName());
    }
  }
}
