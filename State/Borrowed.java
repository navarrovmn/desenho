public class Borrowed extends BookState{
  public Borrowed(){};
  public void solicitar(){
    System.out.println("The book is already borrowed");
  }
  public void devolver(){
    System.out.println("Book was returned");
  }
}
