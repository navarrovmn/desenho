public class Main{
  public static void main(String[] args){
    Book one = new Book("As libelulas do Alem");
    BookState available = new Available();
    BookState notAvailable = new Borrowed();
    one.setState(available);
    System.out.println("Comportamentos com o estado disponivel");
    one.solicitar();
    one.devolver();
    System.out.println("Comportamentos com o estado indisponivel");
    one.setState(notAvailable);
    one.solicitar();
    one.devolver();
  }
}
