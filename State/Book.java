public class Book {
  private String name;
  private BookState bookState;

  public Book(String name){
    this.name = name;
  }
  public String getNome(Book book){
    return name;
  }
  public void setNome(String name){
    this.name = name;
  }
  public void setState(BookState state){
    this.bookState = state;
  }
  public void solicitar(){
    this.bookState.solicitar();
  }
  public void devolver(){
    this.bookState.devolver();
  }

}
