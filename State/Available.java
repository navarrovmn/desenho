public class Available extends BookState{
  public Available(){};
  public void solicitar(){
    System.out.println("The book was borrowed");
  }
  public void devolver(){
    System.out.println("The book cannot be returned because it is available");
  }
}
